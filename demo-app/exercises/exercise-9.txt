Exercise 9

1. Add a new column header to the Car Table named 'Actions'.

2. In the ViewCarRow component add a new column to the row which has a 'Delete' button.

3. When the button is clicked remove the car from the array.

4. Ensure it works.