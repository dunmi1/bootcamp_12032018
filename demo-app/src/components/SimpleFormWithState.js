import React, { useState, useReducer } from 'react';

export const SimpleFormWithReducer = () => {

  const [ state, dispatch ] = useReducer((state, action) => {
    if (action.type === 'INPUT_MESSAGE') {
      return { ...state, message: action.payload };
    }
  }, { message: '' });

  return <form>
    <div>
      <label>Message:</label>
      <input type="text" value={state.message}
        onChange={e => dispatch({ type: 'INPUT_MESSAGE', payload: e.target.value })} />
    </div>
    <div>Message State Value: {state.message}</div>
  </form>;
};


export const SimpleFormWithHooks = () => {

  const [ message, setMessage ] = useState('');

  return <form>
    <div>
      <label>Message:</label>
      <input type="text" value={message} onChange={e => setMessage(e.target.value)} />
    </div>
    <div>Message State Value: {message}</div>
  </form>;
};

export class SimpleFormWithState extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
    };
  }

  change = e =>
    this.setState({ message: e.target.value }, () => {
      console.log('new state message: ' + this.state.message);
    });

  render() {
    return (
      <form>
        <div>
          <label>Message:</label>
          <input type="text" value={this.state.message} onChange={this.change} />
        </div>
        <div>Message State Value: {this.state.message}</div>
      </form>
    );
  }
}
