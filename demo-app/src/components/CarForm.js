import React from 'react';

export class CarForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = this.createCarForm();

    this.change = this.change.bind(this);
  }

  change(e) {
    this.setState({
      [ e.target.name ]: e.target.type === 'number' ? Number(e.target.value) : e.target.value,
    });
  };


  createCarForm() {
    return {
      make: '',
      model: '',
      year: 1900,
      color: '',
      price: 0,
    };
  }

  change = e => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number' ? Number(e.target.value) : e.target.value,
    });
  };

  submitCar = () => {
    this.props.onSubmitCar({ ...this.state });
    this.setState(this.createCarForm());
  };

  render() {
    return <form>
      <div>
        <label id="make-input">Make:</label>
        <input type="text" id="make-input" name="make" value={this.state.make} onChange={this.change} />
      </div>
      <div>
        <label id="model-input">Model:</label>
        <input type="text" id="model-input" name="model" value={this.state.model} onChange={this.change} />
      </div>
      <div>
        <label id="year-input">Year:</label>
        <input type="number" id="year-input" name="year" value={this.state.year} onChange={this.change} />
      </div>
      <div>
        <label id="color-input">Color:</label>
        <input type="text" id="color-input" name="color" value={this.state.color} onChange={this.change} />
      </div>
      <div>
        <label id="price-input">Price:</label>
        <input type="number" id="price-input" name="price" value={this.state.price} onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitCar}>{this.props.buttonText}</button>
    </form>;
  }


}