import React from 'react';

export class EditCarRow extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      make: props.car.make,
      model: props.car.model,
      year: props.car.year,
      color: props.car.color,
      price: props.car.price,
    };
  }

  change = e => {
    this.setState({
      [ e.target.name ]: e.target.value,
    });
  };

  saveCar = () => {
    this.props.onSaveCar({
      ...this.state,
      id: this.props.car.id,
    });
  };

  render() {

    const { car: { carId }, onCancelCar } = this.props;

    return <tr>
      <td>{carId}</td>
      <td><input type="text" name="make" value={this.state.make} onChange={this.change} /></td>
      <td><input type="text" name="model" value={this.state.model} onChange={this.change} /></td>
      <td><input type="number" name="year" value={this.state.year} onChange={this.change} /></td>
      <td><input type="text" name="color" value={this.state.color} onChange={this.change} /></td>
      <td><input type="number" name="price" value={this.state.price} onChange={this.change} /></td>
      <td>
        <button type="button" onClick={this.saveCar}>Save</button>
        <button type="button" onClick={onCancelCar}>Cancel</button>
      </td>
    </tr>;
  }

}