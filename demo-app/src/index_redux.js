import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, bindActionCreators } from 'redux';
import { connect, Provider } from 'react-redux';


const createAddAction = payload => ({ type: 'ADD', payload });
const createSubtractAction = payload => ({ type: 'SUBTRACT', payload });
const createMultiplyAction = payload => ({ type: 'MULTIPLY', payload });
const createDivideAction = payload => ({ type: 'DIVIDE', payload });

const calcReducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD':
      return state + action.payload;
    case 'SUBTRACT':
      return state - action.payload;
    case 'MULTIPLY':
      return state * action.payload;
    case 'DIVIDE':
      return state / action.payload;
    default:
      return state;
  }

};

// const createStore = (reducer) => {

//   let currentState;
//   const subscribers = [];

//   return {
//     getState: () => currentState,
//     dispatch: action => {
//       currentState = reducer(currentState, action);
//       subscribers.forEach(cbFn => cbFn());
//     },
//     subscribe: cbFn => {

//       subscribers.push(cbFn);

//       return () => {
//         const cbFnIndex = subscribers.indexOf(cbFn);
//         subscribers.splice(cbFnIndex, 1);
//       };
//     },
//   };

// };

const calcStore = createStore(calcReducer);

class CalcTool extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      numInput: 0,
    };
  }

  change = e => this.setState({ numInput: Number(e.target.value) });

  render() {
    return <form>
      <div>
        Result: {this.props.result}
      </div>
      <div>
        <label htmlFor="num-input">Input:</label>
        <input type="text" value={this.state.numInput} onChange={this.change} />
      </div>
      <div>
        <button type="button" onClick={() => this.props.add(this.state.numInput)}>+</button>
        <button type="button" onClick={() => this.props.subtract(this.state.numInput)}>-</button>
        <button type="button" onClick={() => this.props.multiply(this.state.numInput)}>*</button>
        <button type="button" onClick={() => this.props.divide(this.state.numInput)}>/</button>
      </div>
    </form>;
  }

}

// const bindActionCreators = (actions, dispatch) => {

//   return Object.keys(actions).reduce( (boundActions, actionKey) => {

//     boundActions[actionKey] = (...params) => dispatch(actions[actionKey](...params));
//     return boundActions;

//   }, {});

// };

// const { Provider, Consumer } = React.createContext(null);

// const connect = (mapStateToPropsFn, mapDispatchToPropsFn) => {
//   return PresentationalComponent => {
//     class ContainerComponent extends React.Component {
//       componentDidMount() {
//         this.dispatchProps = mapDispatchToPropsFn(this.props.store.dispatch);

//         this.unsubscribeFn = this.props.store.subscribe(() => {
//           this.forceUpdate();
//         });

//       }
//       componentWillUnmount() {
//         this.unsubscribeFn();
//       }
//       render() {
//         return <PresentationalComponent {...this.dispatchProps} {...mapStateToPropsFn(this.props.store.getState())} />;
//       }
//     }

//     return () => <Consumer>{value => <ContainerComponent store={value} />}</Consumer>;
//   };
// }

const CalcToolContainer = connect(
  state => ({ result: state }),
  dispatch => bindActionCreators({
    add: createAddAction, subtract: createSubtractAction,
    multiply: createMultiplyAction, divide: createDivideAction,
  }, dispatch),
)(CalcTool);

ReactDOM.render(
  <Provider store={calcStore}>
    <CalcToolContainer />
  </Provider>,
  document.querySelector('#root')
);

// calcStore.dispatch({ type: 'INIT' });



