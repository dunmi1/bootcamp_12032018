import {
  createStore,
  applyMiddleware,
  combineReducers,
} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import {
  loadingReducer,
  editCarIdReducer,
  carsReducer,
} from './reducers/carToolReducer';

export const carToolStore = createStore(
  combineReducers({
    loading: loadingReducer,
    editCarId: editCarIdReducer,
    cars: carsReducer,
  }),
  composeWithDevTools(applyMiddleware(thunk)),
);