import { refreshCars } from './refreshCars';

export const DELETE_CAR_REQUEST = 'DELETE_CAR_REQUEST';
// export const DELETE_CAR_DONE = 'DELETE_CAR_DONE';

export const createDeleteCarsRequestAction = (carId) =>
  ({ type: DELETE_CAR_REQUEST, payload: carId });
// export const createDeleteCarsDoneAction = () =>
//   ({ type: DELETE_CAR_DONE });

export const deleteCar  = (carId) => {

  return dispatch => {

    dispatch(createDeleteCarsRequestAction(carId));
    return fetch('http://localhost:3050/cars/' + carId, { method: 'DELETE' })
      .then(res => res.json())
      .then(() => dispatch(refreshCars()));
  };
};
