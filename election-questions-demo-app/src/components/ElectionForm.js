import React from 'react';


export class ElectionForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      electionName: '',
      question: '',
    }
  }

  change = e => { this.setState({ [e.target.name]: e.target.value })}

  saveElection = () => {
    this.props.onSaveElection({
      name: this.state.electionName,
      questions: this.props.questions.concat(),
    });
  }

  render() {

    return <form>

      <div>Election Count: {this.props.elections.length}</div>

      <div>
        <label>Election Name:</label>
        <input type="text" name="electionName" value={this.state.electionName} onChange={this.change} />
      </div>

      <ul>
        {this.props.questions.map(question => <li>{question}</li>)}
      </ul>

      <div>
        <label>New Question:</label>
        <input type="text" name="question" value={this.state.question} onChange={this.change} />
        <button type="button" onClick={() => this.props.onAddQuestion(this.state.question)}>Add Question</button>
      </div>

      <button type="button" onClick={this.saveElection}>Save Election</button>

    </form>;

  }


}