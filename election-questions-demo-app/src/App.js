import React, { Component } from 'react';
import { Provider, connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import { appStore, refreshElections, saveElection, createAddQuestionAction } from './appStore';
import { ElectionForm } from './components/ElectionForm';

export const ElectionFormContainer = connect(
  ({ elections, questions }) => ({ elections, questions }),
  dispatch => bindActionCreators({
    onRefreshElections: refreshElections,
    onAddQuestion: createAddQuestionAction,
    onSaveElection: saveElection,
  }, dispatch),
)(ElectionForm);

class App extends Component {
  render() {
    return (
      <Provider store={appStore}>
        <ElectionFormContainer />
      </Provider>
    );
  }
}

export default App;
