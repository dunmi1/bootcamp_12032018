import { createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools} from 'redux-devtools-extension';

export const createAddQuestionAction = (question) => ({ type: 'ADD_QUESTION', payload: question });

export const createSaveElectionRequestAction = (election) => ({ type: 'SAVE_ELECTION_REQUEST', payload: election });

export const createRefreshElectionsRequestAction = () => ({ type: 'REFRESH_ELECTIONS_REQUEST' });

export const createRefreshElectionsDoneAction = (elections) => ({ type: 'REFRESH_ELECTIONS_DONE', payload: elections });

export const refreshElections = () => {
  return dispatch => {

    dispatch(createRefreshElectionsRequestAction());

    return fetch('http://localhost:3050/elections')
      .then(res => res.json())
      .then(elections => dispatch(createRefreshElectionsDoneAction(elections)));


  }
}

export const saveElection = (election) => {
  return dispatch => {

    dispatch(createSaveElectionRequestAction(election));

    Promise.all(election.questions.map(question => {
      return fetch('http://localhost:3050/questions', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ text: question })
      }).then(res => res.json()).then(question => question.id);
    })).then(questionIds => {
      const newElection = { name: election.name, questions: questionIds };

      return fetch('http://localhost:3050/elections', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newElection),
      })
        .then(() => dispatch(refreshElections()));
    });


    
  }
}



const electionsReducers = (state = [], action) =>{


};

const questionsReducer = (state = [], action) => {


  return state;

}

const electionFormReducer = (state = { elections: [], questions: [] }, action) => {

  if (action.type === 'REFRESH_ELECTIONS_DONE') {
    return { ...state, elections: action.payload };
  }

  if (action.type === 'SAVE_ELECTION_REQUEST') {
    return { ...state, questions: [] };
  }


  if (action.type === 'ADD_QUESTION') {
    return { ...state, questions: state.questions.concat(action.payload) };
  }


  return state;



};

// combineReducers({
//   elections: electionsReducers,
//   questions: questionsReducer
// })

export const appStore = createStore(electionFormReducer, composeWithDevTools(applyMiddleware(thunk)));