Exercise 4

1. Implement Widget Form (copy from GitLab) to collect new widget data and add to the server using a GraphQL mutation. Ensure the Widget List is updated. Use the refetch queries strategy for updating the widget list.

2. Using your Car Form from earlier in the class, collect car data and add it to the Car graphql server using a mutation. Ensure the Car Table is updated. For the mutation, use an optimistic response and update function to update the cache.

3. Add a delete button to each row of the car table, when the button is clicked use a mutation to delete car. To update the table of cars, use an update function only - no refetch queries, no optimistic update.

4. Ensure both Widget Tool and Car Tool.