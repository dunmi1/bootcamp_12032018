export const typeDefs = `
  type Query {
    message: String
    widgets: [Widget]
  }

  type Mutation {
    appendWidget(widget: WidgetInput): Widget
    deleteWidget(widgetId: Int): Int
  }

  type Widget {
    id: Int
    name: String
    description: String
    color: String
    size: String
    price: Float
    quantity: Int
  }

  input WidgetInput {
    id: Int
    name: String
    description: String
    color: String
    size: String
    price: Float
    quantity: Int
  }
`;
