import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

export const DELETE_WIDGET_MUTATION = gql`
  mutation DeleteWidgetMutation($widgetId: ID) {
    deleteWidget(widgetId: $widgetId) {
      id
    }
  }
`;

// Enhancer - only has knowledge of the Apollo/GraphQL environment, some requirements on props passed into
export const withDeleteWidgetMutation = graphql(DELETE_WIDGET_MUTATION, {
  props: ({ mutate, ownProps }) => {

    // new props we want to add to ownProps
    return {
      onDeleteWidget: widgetId => {

        return mutate({
          variables: { widgetId },
          update(store, { data: { deleteWidget: widget } }) {
            const data = store.readQuery({ query: ownProps.widgetRefreshQuery });
            const widgetIndex = data.widgets.findIndex(w => w.id === widget.id);
            data.widgets.splice(widgetIndex, 1);
            store.writeQuery({ query: ownProps.widgetRefreshQuery, data });
          },
        }).then(() => ownProps.onCancelWidget());
  
      }
    };
  },
});
