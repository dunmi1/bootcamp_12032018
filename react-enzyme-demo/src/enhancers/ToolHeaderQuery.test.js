import * as React from "react";
import { mount } from 'enzyme';
import { MockedProvider } from "react-apollo/test-utils";
import wait from "waait";

import { withToolHeaderQuery, TOOL_HEADER_QUERY } from "./ToolHeaderQuery";

const ToolHeaderContainer = withToolHeaderQuery(props => <header>
  <h1>{props.headerText}</h1>
</header>)

describe('ToolHeaderQuery Tests', () => {

  test("should render without error", async () => {

    const toolHeaderQueryMock = {
      request: { query: TOOL_HEADER_QUERY },
      result: { data: { app: { id: '1', toolName: 'Test Tool' } } }
    };

    const component = mount(
      <MockedProvider mocks={[toolHeaderQueryMock]} addTypename={false}>
        <ToolHeaderContainer />
      </MockedProvider>
    );
    
    expect(component.find('h1').text()).toBe('');

    await wait(0);

    expect(component.find('h1').text()).toBe('Test Tool');

  });
  
});
