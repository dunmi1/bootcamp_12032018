import AppData from './AppData';

jest.mock('./WidgetData');

describe('AppData Mocking Demo', () => {

  test('Get All Widgets', () => {

    const appData = new AppData();

    return appData.allWidgets().then(widgets => expect(widgets.length).toEqual(2));
  });

});