import * as React from 'react';
import * as PropTypes from 'prop-types';

export const WidgetViewRow = ({ widget, onDeleteWidget, onEditWidget }) =>
  <tr>
    <td>{widget.id}</td>
    <td>{widget.name}</td>
    <td>{widget.description}</td>
    <td>{widget.color}</td>
    <td>{widget.size}</td>
    <td>{widget.price}</td>
    <td>{widget.quantity}</td>
    <td>
      <button type="button" onClick={() => onEditWidget(widget.id)}>Edit</button>
      <button type="button" onClick={() => onDeleteWidget(widget.id)}>Delete</button>
    </td>
  </tr>;

WidgetViewRow.fragments = {
  widgetRow: `
    fragment widgetRow on Widget {
      id
      name
      description
      color
      size
      price
      quantity
    }
  `,
};

WidgetViewRow.propTypes = {
  widget: PropTypes.object.isRequired,
  onDeleteWidget: PropTypes.func.isRequired,
  onEditWidget: PropTypes.func.isRequired,
};
