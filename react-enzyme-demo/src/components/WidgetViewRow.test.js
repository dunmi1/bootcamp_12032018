import * as React from 'react';
import { render, mount, shallow } from 'enzyme';

import { WidgetViewRow } from './WidgetViewRow';

describe('<WidgetViewRow /> Enzyme Static HTML', () => {

  let widget;

  beforeEach(() => {

    widget = {
      id: 1,
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.00,
      quantity: 20,
    };

  });

  test('<WidgetViewRow /> renders', () => {
    
    const component = JSON.stringify(render(
      <table>
        <thead>
          <WidgetViewRow widget={widget} onDeleteWidget={() => null} onEditWidget={() => null} />
        </thead>
      </table>
    ).html());
    
    expect(component).toMatchSnapshot();
  });

});

describe('<WidgetViewRow /> Enzyme Mock DOM', () => {

  const eventHandlers = {
    deleteWidget: () => null,
    editWidget: () => null,
  };

  let widget;
  let deleteWidgetSpy;
  let editWidgetSpy;
  let component;

  beforeEach(() => {

    widget = {
      id: 1,
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.00,
      quantity: 20,
    };

    deleteWidgetSpy = jest.spyOn(eventHandlers, 'deleteWidget');
    editWidgetSpy = jest.spyOn(eventHandlers, 'editWidget');

    component = mount(<table><thead>
      <WidgetViewRow widget={widget}
        onDeleteWidget={eventHandlers.deleteWidget}
        onEditWidget={eventHandlers.editWidget} />
    </thead></table>).find(WidgetViewRow);

  });

  test('<WidgetViewRow /> renders', () => {

    const columns = ['id', 'name', 'description', 'color', 'size', 'price', 'quantity'];

    component.find('td').slice(0,6).forEach( (node, index) => {
      const widgetField = String(widget[columns[index]]);
      expect(node.text()).toBe(widgetField);
    } );

  });

  test('<WidgetViewRow /> delete widget button', () => {

    component.find('button').first().simulate('click');
    component.find('button').last().simulate('click');

    expect(deleteWidgetSpy).toHaveBeenCalledWith(widget.id);
    expect(editWidgetSpy).toHaveBeenCalledWith(widget.id);

  });

});

describe('<WidgetViewRow /> Shallow with Enzyme', () => {

  const eventHandlers = {
    deleteWidget: () => null,
    editWidget: () => null,
  };

  let widget;
  let deleteWidgetSpy;
  let editWidgetSpy;
  let wrapper;

  beforeEach(() => {

    widget = {
      id: 1,
      name: 'Test Widget',
      description: 'Test Widget Description',
      color: 'red',
      size: 'small',
      price: 10.00,
      quantity: 20,
    };

    deleteWidgetSpy = jest.spyOn(eventHandlers, 'deleteWidget');
    editWidgetSpy = jest.spyOn(eventHandlers, 'editWidget');

    wrapper = shallow(<WidgetViewRow widget={widget}
      onDeleteWidget={eventHandlers.deleteWidget}
      onEditWidget={eventHandlers.editWidget} />);

  });

  test('<WidgetViewRow /> renders', () => {

    const columns = ['id', 'name', 'description', 'color', 'size', 'price', 'quantity'];

    wrapper.find('td').slice(0,6).forEach( (node, index) => {
      const widgetField = String(widget[columns[index]]);
      expect(node.text()).toBe(widgetField);
    } );

  });

  test('<WidgetViewRow /> buttons', () => {

    wrapper.find('button').first().simulate('click');
    wrapper.find('button').last().simulate('click');

    expect(deleteWidgetSpy).toHaveBeenCalledWith(widget.id);
    expect(editWidgetSpy).toHaveBeenCalledWith(widget.id);

  });



});

