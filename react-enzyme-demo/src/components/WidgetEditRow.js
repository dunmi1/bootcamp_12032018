import * as React from 'react';

export class WidgetEditRow extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      name: props.widget.name,
      description: props.widget.description,
      color: props.widget.color,
      size: props.widget.size,
      price: props.widget.price,
      quantity: props.widget.quantity,
    };
  }

  change = evt => {
    this.setState({
      [ evt.target.name ]: evt.target.type === 'number'
        ? Number(evt.target.value)
        : evt.target.value,
    });
  };

  saveWidget = () => {
    console.log(this.props);
    this.props.onSaveWidget({
      ...this.state,
      id: this.props.widget.id,
    });
  };

  render() {
    return <tr>
      <td>{this.props.widget.id}</td>
      <td>
        <input type="text" name="name" value={this.state.name} onChange={this.change} />
      </td>
      <td>
        <input type="text" name="description" value={this.state.description} onChange={this.change} />
      </td>
      <td>
        <input type="text" name="color" value={this.state.color} onChange={this.change} />
      </td>
      <td>
        <input type="text" name="size" value={this.state.size} onChange={this.change} />
      </td>
      <td>
        <input type="number" name="price" value={this.state.price} onChange={this.change} />
      </td>
      <td>
        <input type="number" name="quantity" value={this.state.quantity} onChange={this.change} />
      </td>
      <td>
        <button type="button" onClick={this.saveWidget}>Save</button>
        <button type="button" onClick={this.props.onCancelWidget}>Cancel</button>
      </td>
    </tr>;
  }

}